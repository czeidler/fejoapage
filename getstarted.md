---
layout: page
title: "Get Started: Download, Build and Run Fejoa"
---

## Download Sources
The Fejoa source code is hosted on [GitLab](https://gitlab.com/czeidler/fejoa) and can be obtained using git:

`git clone git@gitlab.com:czeidler/fejoa.git`

Fejoa uses Kotlin's multi platform builds to support the JVM and JavaScript.
For this reason a module is usually split into a common, a JVM and a JS part.
Only modules that just run on the JVM have a single module directory, e.g. the server.

## Build and Run Fejoa

*NOTE: Fejoa is not ready to be used in production yet and should only be run for developing and testing purposes.*

Fejoa uses gradle as a build system.
Before the first build you may need to initialize the gradle wrapper by running:

`gradle wrapper`


### Run JVM Tests
To run the JVM tests do 

`./gradlew build -p {module}JVM`

where `{module}JVM` must be a JVM module (e.g. fejoaJVM, CloudEFSJVM).

### Run JS Tests
To run the JavaScript tests do

`./gradlew karmaRun -p {module}JS `

This will start a browser to run the tests (either Firefox or Chrome).
When changing the karma config in the `build.gradle` file, e.g. to change the test browser, the `karmaRefresh` gradle target needs to be executed.

Note, no all crypto tests success when running in the browser.
This is due to browser incompatibilities when doing elliptic curve (EC) cryptography.
However, currently no EC is used on default.

### Server

To build the Fejoa server running

`./gradlew jar -p server`

This will produce an executable jar file in `server/build/libs`.
For example, 

`java -jar server-1.0-SNAPSHOT.jar -d . -h localhost -p 8080`

starts the server on `localhost:8080` and uses the current directory as data storage.

### Command Line Interface

The Fejoa command line interface can be build with

`./gradlew jar -p cli`

The CLI has two parts.
First, a daemon that managed the current state, e.g. if a user is logged in.
Secondly, a client that communicates with the daemon.
The daemon is started automatically when executing a client command.
Moreover the daemon shuts down automatically after a certain timeout (This timeout is very low at the moment...).

To use the client add the following lines to your `.bash_aliases`:

```
FEJOA_CLI_PATH=~/path_to_the_source/fejoa/cli

function fejoa () {
	$FEJOA_CLI_PATH/fejoa.sh $@
}

function fejoadaemon () {
	$FEJOA_CLI_PATH/daemon.sh
}
```

After restarting your bash the client can be started with the `fejoa` command.

