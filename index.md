---
layout: home
---

<section id="about">
	<div class="container">

<section markdown="1" class="blogpost">
{% include main.md %}
</section>

		<div class="panel panel-default">
			<div class="panel-body">
				<div class="blogpost">
					{% for post in site.posts limit: 2 %}
						<li><span>{{ post.date | date_to_string }}</span> &raquo; <a href=".{{ post.url }}">{{ post.title }}</a></li>
					{{ post.content | strip_html | truncatewords:75}}<br>
						<a href=".{{ post.url }}">Read more...</a><br><br>
					{% endfor %}
				</div>
			</div>
		</div>
		{% include social.html %}
	</div>
</section>
