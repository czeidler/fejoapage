Fejoa is an open source data sharing platform for sharing data with contacts at the same or a different service provider.
Fejoa allows users to migrate their data/account to a different service provider without losing their contacts.

<center>
<video width="600" height="400" autoplay loop>
	<source src="{{ site.baseurl }}/static/video/{{ site.introvideo }}" type="video/mp4">
	Your browser does not support the video tag.
</video>
</center>

To preserve the privacy of users, the data in Fejoa is concealed from the service provider by employing encryption techniques.

**Fejoa is coming soon!**

Follow the development on [GitLab](https://gitlab.com/czeidler/fejoa).

### Publications
- Privacy-Preserving Data Sharing in Portable Clouds [pdf](static/pdf/Zeidler-2016-PortableCloud.pdf)
- CloudEFS: Efficient and Secure File System for Cloud Storage [pdf](static/pdf/Zeidler-2016-CloudEFS.pdf)
- AuthStore: Password-based Authentication and Encrypted Data Storage in Untrusted Environments [pdf (draft)](static/pdf/Zeidler-2017-AuthStore-Draft.pdf)
