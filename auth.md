---
layout: auth
title: FejoaAuth
---

FejoaAuth allows secure password reuse at multiple FejoaAuth providers.
FejoaAuth prevent providers to learn any information about the user password and thus a malicious provider can't impersonate the user at another provider or decrypt data that is protected with the user password.

FejoaAuth uses key strengthening to protect the user password from dictionary attacks.
The user is in control of choosing appropriate key strengthening parameters to make it arbitrary hard for an attacker to brute force the user password.

To prevent leaking the user password during the authentication process FejoaAuth uses the CompactPake protocol.
CompactPake is secure against parameter attacks (see [paper draft](static/pdf/Zeidler-2017-AuthStore-Draft.pdf)) and reveals no information about the entered passwords.
For example, if a user accidentally attempts to login at a provider with a password used at another provider, the current provider can't uses the exchanged information to login at the other provider or learn information about the password.

## What's wrong with current password-based authentication?

As already discussed in an earlier [post]({{ site.baseurl }}{% post_url 2017-04-03-Authentication %}), traditional password-based web authentication fails to prevent web page providers to learn the user password.
For example, the password is either send in plain text to the web page provider when submitting the login form or the password can be recorded straight from the input field using JavaScript.

As a result an authentication password should never be used for another purpose such as data encryption or to authenticate at another service.
However, maintaining multiple secure passwords is cumbersome and neglected by many people.
For this reason a solution is needed that requires users to only remember one single password.

A promising solution is a password protected password managers.
However, up to today password managers suffered from a Hen and Egg problem as described below.
Storing a password manager locally/offline is probably the most secure way to protect it from an attackers in the web.
However, keeping it locally makes it vulnerable to data loss for which reason a user might want to keep a backup of the password manager in the cloud.
This usually requires a password-based registration at a cloud storage provider.
As discussed earlier the authentication password should differ from the password to encrypt the password manager.
This leads us to the Hen and Egg problem; we need to remember two passwords.

While remembering two passwords seems not too much of a problem the reality looks much worse because often user don't even have the choice to choose separate passwords for authentication and encryption.
For example, 1password not only is able to record the encryption key at account creation but is able to learn the master password at every login on their webpage.
The same problem exists for cloud storage providers that offer client side encryption.

FejoaAuth solves all these problems.
Moreover, users are in control of how secure their passwords are protected.
For example, users don't need to trust provides anymore to not store their password in plain text.


## FejoaAuth Browser Extension

Using conventional password-based authentication on the web, users can't prevent web page providers to learn the entered password.
For example, a web page provider can simply add JS code to readout the plain user password and even record passwords while typing.

To provide secure password-based authentication on web pages we developed a FejoaAuth browser extension that allows users to authenticate at supported FejoaAuth providers.

<center>
<img src="{{ site.baseurl }}/static/png/auth/RegisterPopup.png"/>
</center>

<center>
<img src="{{ site.baseurl }}/static/png/auth/SecuritySettings.png"/>
</center>

When registration at a supported web page the extension let the user choose how secure their passwords are protected.
The build in benchmark gives users an estimate of how much time is needed to strengthen their password, i.e. how long a login attempt will take.

### Password Manager
To provide a complete password-based authentication solution the extension includes a password manager.
The password manager uses FejoaAuth storage to securely store user passwords.

<center>
<img src="{{ site.baseurl }}/static/png/auth/StorePasswordQuestion.png"/>
</center>

### Build
To initialize the gradle wrapper run:

`gradle wrapper`

To build the browser extension run:

`./gradlew build -p browseraddon`

This will output the extension at:

`browseraddon/build/addon`

This extension directory can then be loaded into Chrome or Firefox (untested).


To build and run the FejoaAuth server run:

`./gradlew runServer`

The server will host a test authentication page at:

`http://localhost:8080/web/TestPage.html`



