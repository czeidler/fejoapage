---
layout: post
title: "FejoaAuth and Multi-platform Status"
tags:
- Blog
- Post
--- 

## FejoaAuth

I'm happy to introduce FejoaAuth, a browser addon that allows secure password-based web authentication without leaking the user password to the web page provider.
Furthermore, FejoaAuth comes with a build in password manager to securely store third party passwords.

FejoaAuth makes it possible to use a single password to securely protect the password manager (encrypt it) and to back it up in the cloud.
In other words, the same password can be used for data encryption and authentication at a storage provider where the password manager should be stored.
With other solution this is not the case since the authentication password can easily be intercepted by the provider during a web login and then used to decrypt the password manager.
More information about the FejoaAuth browser addon can be found [here]({{ site.baseurl }}/auth).

<center>
<img src="{{ site.baseurl }}/static/png/auth/RegisterPopup.png"/>
</center>

FejoaAuth was born out of the need for a secure web-based authentication method for Fejoa.
In the future FejoaAuth will be used to provide a web-based interface to manage Fejoa user accounts.
For example, to register, modify or delete an account.


## Common Code for JVM and JavaScript
The CloudEFS and the Fejoa client code now fully compiles to JVM and JavaScript (thanks to Kotlin's multi-platform support).
As a first application of this cross platform architecture is FejoaAuth.
The FejoaAuth addon uses the same Kotlin code for the authentication protocol CompactPAKE as it is used in the JVM.
Furthermore, FejoaAuth uses CloudEFS to securely store passwords in the password manager.
This also simplifies the password manager synchronization across one or more storage providers since only one code base needs to be maintained.

Crypto primitives such as hashes, AES encryption or key derivation functions (KDF) are provided by the SubtleCrypto interface when running in the browser and by BouncyCastle when running in the JVM.
Since SubtleCrypto only supports PDKDF2 as a key derivation function, FejoaAuth currently only supports this KDF.
Support for more advanced KDFs, such as bcrypt, scrypt or Argon2, is planned in the future.
