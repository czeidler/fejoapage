---
layout: post
title: "Enhanced Privacy Preserving Data Encryption"
tags:
- Blog
- Post
---

This post gives some information about the advantages of the encryption system used in Fejoa.
Fejoa uses an encrypted storage system called CloudEFS ([pdf](static/pdf/CloudEFS.pdf)).
CloudEFS leaks less information than traditional file based encryption systems, i.e. it does not leak any meta data such as the directory structure, the number of files or the file size.

Typically a system that does per file encryption encrypts the file content and the file name:

<center>
<img width="500" src="{{ site.baseurl }}/static/png/FileBasedFSEncryption.png"/>
</center>

This means an attacker can still learn certain properties such as the used file structure, the number of files and the size of the files.
From this information the attacker might be able to guess what kind of data is stored.
For example, if there are many files with a size around ~2Mb; this might be a user's picture collection.
Similar, if the attacker has a rough idea about what a user is working on, e.g. a software project, the attacker may learn the status of the project, the used programming language or the used build system.
While these examples sound quite harmless one can easily imagine a situation were leaking such information becomes awkward.

CloudEFS, the storage system used in Fejoa uses a different approach.
CloudEFS chunks all data and meta data, such as the directory structure, into a list of data chunks.
These chunks are encrypted and an attacker does not learn how chunks are related.
All chunks are stored in a chunk store:

<center>
<img width="500" src="{{ site.baseurl }}/static/png/ChunkStoreEncryption.png"/>
</center>

Only the data owner knows how to navigate through though this list of chunks.
To do so data in CloudEFS is organized in chunk containers.
Each chunk container is a tree structure and all nodes and all data leafs in this tree are again encrypted chunks.
Starting from a *root chunk* the user can navigate the chunk container to access the stored data.

In CloudEFS, file system objects such as directories, files and commits are stored in chunk containers.
This means knowing the root chunk that points to the latest version of the file system allows the data owner to navigate the whole file system as well as the history of the file system.

<center>
<img width="500" src="{{ site.baseurl }}/static/png/ObjectModel.png"/>
</center>

This technique makes it possible that virtually no file system meta data is leaked to the storage provider.
