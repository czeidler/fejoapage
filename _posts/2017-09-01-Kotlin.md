---
layout: post
title: "Using Kotlin in Fejoa"
tags:
- Blog
- Post
---

Fejoa is now using Kotlin for the core parts of its implementations.
There are various reasons for the move from Java to a mix of Java and Kotlin.
One reason is that Kotlin supports coroutines and makes it easy to write asynchronous code in an synchronous fashion.
For example, the previously used CompletableFuture API made it cumbersome to write/read conditional logic or to do complex error handling (at least in my opinion).
Furthermore, using Kotlin's coroutines makes it much clearer to see in which context some code is running, e.g if its running in the worker or in the UI thread.
Another reason is that Kotlin compiles to Java byte code as well as to Java Script what makes it possible to use the same code base for desktop, Android and browser apps.

The planned Java Script support required various API changes, e.g. because browser encryption and storage APIs are asynchronous the Fejoa code that uses these APIs needs to be asynchronously as well.
For example, to run Fejoa in a browser app it is planned to use IndexedDB as a storage backend for CloudEFS.
However, all IO in IndexedDB is done asynchronously what makes it impossible to use the old synchronous CloudEFS code.
For this reason CloudEFS has been ported to use a asynchronous storage backend so that the same code can be used for JVM as well as for Java Script.
Thanks to Kotlin's asynchronous suspend functions this was actually not too difficult and most existing code was reused.

A good outcome of the whole porting process is that many parts of Fejoa have been revised and update.
So far only CloudEFS and a few parts of the original Java codebase have been ported.
In a next step the rest of Fejoa needs to be adapted to work with the new APIs.
This includes:

* the Fejoa core library
* the Fejoa command line interface
* the Fuse file system driver




 
